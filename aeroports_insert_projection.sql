-- FUNCTION: flymap.aeroports_insert_projection()

-- DROP FUNCTION flymap.aeroports_insert_projection();

CREATE FUNCTION flymap.aeroports_insert_projection()
    RETURNS trigger
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE NOT LEAKPROOF
AS $BODY$
DECLARE
   srid integer := nextval('aviation.aeroports_srid_seq');
BEGIN

INSERT INTO spatial_ref_sys (srid, auth_name, proj4text) VALUES (srid, CONCAT('azimuthal equidistant ', NEW.ville), CONCAT('+proj=aeqd +lat_0=', ST_Y(NEW.localisation), ' +lon_0=', ST_X(NEW.localisation), ' +x_0=0 +y_0=0 +a=6371000 +b=6371000 +units=m +no_defs') );

NEW.srid := srid;

RETURN NEW;

END
$BODY$;

ALTER FUNCTION flymap.aeroports_insert_projection()
    OWNER TO iutsd;
