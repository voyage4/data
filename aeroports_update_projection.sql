-- FUNCTION: flymap.aeroports_update_projection()

-- DROP FUNCTION flymap.aeroports_update_projection();

CREATE FUNCTION flymap.aeroports_update_projection()
    RETURNS trigger
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE NOT LEAKPROOF
AS $BODY$
BEGIN

UPDATE spatial_ref_sys SET auth_name = CONCAT('azimuthal equidistant ', NEW.ville), proj4text = CONCAT('+proj=aeqd +lat_0=', ST_Y(NEW.localisation), ' +lon_0=', ST_X(NEW.localisation), ' +x_0=0 +y_0=0 +a=6371000 +b=6371000 +units=m +no_defs') WHERE srid = NEW.srid;

RETURN NEW;

END
$BODY$;

ALTER FUNCTION flymap.aeroports_update_projection()
    OWNER TO iutsd;
